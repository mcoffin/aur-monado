# Maintainer: Daniel Bermond <dbermond@archlinux.org>

pkgbase=monado
pkgname=('monado' 'monado-doc')
pkgver=21.0.0
pkgrel=11
pkgdesc='An open source OpenXR runtime'
arch=('x86_64')
url='https://monado.dev/'
license=('Boost')
makedepends=('git' 'meson' 'doxygen' 'eigen' 'glslang' 'qt5-base' 'v4l-utils'
			 'vulkan-headers'
			 'dbus' 'glib2' 'gstreamer' 'gst-plugins-base-libs' 'hidapi' 'libgl'
			 'libjpeg-turbo' 'librealsense' 'libsurvive' 'libusb' 'libuvc' 'libx11'
			 'libxcb' 'opencv' 'openhmd' 'sdl2' 'systemd-libs' 'vulkan-icd-loader'
			 'wayland' 'wayland-protocols' 'zlib')
_srctag="v$pkgver"
source=("git+https://gitlab.freedesktop.org/monado/monado.git#tag=$_srctag"
        '010-monado-meson-0.61.0-fix.patch'::'https://gitlab.freedesktop.org/monado/monado/-/commit/cce20942901211a6820654c31ce86d4bd06ab597.patch'
        '020-monado-gcc-10-fix-part001.patch'::'https://gitlab.freedesktop.org/monado/monado/-/commit/1a556740d5ba79af8f5e72d7145ab62b8867db0b.patch'
        '030-monado-gcc-10-fix-part002.patch'::'https://gitlab.freedesktop.org/monado/monado/-/commit/a0c8cc14f52f1b922c9c4fd1272652e01e130282.patch'
	'survive-wait-hmd.patch'
	'compositor-prefer-direct.patch'
	'ipc-disable-stdin.patch'
	'comp_layer_renderer-fix.patch'
	'render-timing-hack.patch'
	'survive-no-spamlog.patch'
	'FindJPEG.cmake')
bssums=('SKIP'
            '902de0126f2279800acb9bff8a6db9e59932d4eaf8a5a7dcf3e6631a6aa00347'
            '690fbd3f1d8e2f8b71924fc22dbfefe663dfa931741562033a9bcec5d2bb83b7'
            'b1b430610995654ecfd8ea23d27543ba1e716076699fbc2eaf1fb622f111cf3e'
            'fdeee88dad2b21035ee1c2b531562c1d2230ca4fbe29e57bcdd3cda56eb06106'
            '4e776c90dc106dd4000b395a0b04eaef7ce865a5aee42a1c86e8127a8d2f9a5d'
            '810051397bf3df65db9d3c7ea2b12ea53d4efb0b43908901663062ea64933d33'
            'f20ba84a74c4a275b5d4d57197c747f60e0cea96b08d1118f82fac498200170e')
options=('debug' '!strip')

prepare() {
	# cd monado
	# local _f
	# for _f in "${source[@]}"; do
	#     grep -E '\.patch$' <<< "$_f" || continue
	#     patch -Np1 -i "$srcdir/$_f"
	# done
	patch -d monado -Np1 -i "${srcdir}/010-monado-meson-0.61.0-fix.patch"
	patch -d monado -Np1 -i "${srcdir}/020-monado-gcc-10-fix-part001.patch"
	patch -d monado -Np1 -i "${srcdir}/030-monado-gcc-10-fix-part002.patch"
	local _f
	for _f in survive-wait-hmd.patch compositor-prefer-direct.patch ipc-disable-stdin.patch comp_layer_renderer-fix.patch render-timing-hack.patch survive-no-spamlog.patch; do
		patch -d monado -Np1 -i "${srcdir}/$_f"
	done
	mkdir -p "$srcdir"/cmake
	for _f in "$(find "$srcdir" -mindepth 1 -maxdepth 1 -type f,l -name '*.cmake')"; do
		ln -f -s "$_f" "$srcdir/cmake"/"$(basename "$_f" | tr -d '\n')"
	done
}

build() {
	cd "$srcdir"
	if [ -e /opt/libjpeg6 ]; then
		if [ -z "$PKG_CONFIG_PATH" ]; then
			export PKG_CONFIG_PATH='/opt/libjpeg6/lib/pkgconfig:/usr/lib/pkgconfig:/usr/local/lib/pkgconfig'
		else
			export PKG_CONFIG_PATH='/opt/libjpeg6/lib/pkgconfig':"$PKG_CONFIG_PATH":'/usr/lib/pkgconfig:/usr/local/lib/pkgconfig'
		fi
	fi
	msg2 'libjpeg flags:'
	msg2 "$(sh -c 'pkg-config --cflags libjpeg && pkg-config --libs libjpeg' | tr '\n' ' ')"
	# arch-meson -Dinstall-active-runtime='false' build "monado"
	cmake -G Ninja -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_MODULE_PATH="$srcdir/cmake" \
		-DXRT_OPENXR_INSTALL_ACTIVE_RUNTIME=OFF \
		-DBUILD_TESTING=OFF \
		./monado
	ninja -C build -v
}

check() {
	ninja -C build test
}

package_monado() {
	depends=('dbus' 'glib2' 'gstreamer' 'gst-plugins-base-libs' 'hidapi' 'libgl'
                 'libjpeg-turbo' 'librealsense' 'libsurvive' 'libusb' 'libuvc' 'libx11'
                 'libxcb' 'opencv' 'openhmd' 'sdl2' 'systemd-libs' 'vulkan-icd-loader'
                 'wayland' 'zlib')
	install=monado.install
	provides=('openxr-runtime')
	
	DESTDIR="$pkgdir" ninja install -C build
}

package_monado-doc() {
	pkgdesc+=' (documentation)'
	arch=('any')
	
	install -d -m755 "${pkgdir}/usr/share/doc/monado"
	cp -dr --no-preserve='ownership' build/doc/html "${pkgdir}/usr/share/doc/monado"
}
sha256sums=('SKIP'
            '902de0126f2279800acb9bff8a6db9e59932d4eaf8a5a7dcf3e6631a6aa00347'
            '690fbd3f1d8e2f8b71924fc22dbfefe663dfa931741562033a9bcec5d2bb83b7'
            'b1b430610995654ecfd8ea23d27543ba1e716076699fbc2eaf1fb622f111cf3e'
            'fdeee88dad2b21035ee1c2b531562c1d2230ca4fbe29e57bcdd3cda56eb06106'
            '4e776c90dc106dd4000b395a0b04eaef7ce865a5aee42a1c86e8127a8d2f9a5d'
            '810051397bf3df65db9d3c7ea2b12ea53d4efb0b43908901663062ea64933d33'
            'fdb867196f6242b6c2f2692474a6be4b611c0fa952d821b323b52c183c03485b'
            '47e0a0e2c798c5c8ae23872d923baa6c47a9de485808b8f8f70eee726065fc1d'
            '300d1dc0b952fa15a0657a71704cee1b027be2252e9ea0cca6f9903c6ff08e7e'
            'f20ba84a74c4a275b5d4d57197c747f60e0cea96b08d1118f82fac498200170e')
