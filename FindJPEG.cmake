find_package(PkgConfig REQUIRED)
pkg_check_modules(JPEG libjpeg)

find_path(
	JPEG_INCLUDE_DIR jpeglib.h
	HINTS ${JPEG_INCLUDE_DIRS}
)
mark_as_advanced(JPEG_INCLUDE_DIR)
if (JPEG_INCLUDE_DIR)
	file(GLOB _JPEG_CONFIG_HEADERS_FEDORA "${JPEG_INCLUDE_DIR}/jconfig*.h")
	file(GLOB _JPEG_CONFIG_HEADERS_DEBIAN "${JPEG_INCLUDE_DIR}/*/jconfig.h")
	set(_JPEG_CONFIG_HEADERS "${JPEG_INCLUDE_DIR}/jpeglib.h")
	list(APPEND _JPEG_CONFIG_HEADERS "${_JPEG_CONFIG_HEADERS_FEDORA}")
	list(APPEND _JPEG_CONFIG_HEADERS "${_JPEG_CONFIG_HEADERS_DEBIAN}")
	foreach (_JPEG_CONFIG_HEADER IN LISTS _JPEG_CONFIG_HEADERS)
		if (NOT EXISTS "${_JPEG_CONFIG_HEADER}")
			continue ()
		endif()
		file(STRINGS "${_JPEG_CONFIG_HEADER}" jpeg_lib_version REGEX "^#define[\t ]+JPEG_LIB_VERSION[\t ]+.*")
		if (NOT jpeg_lib_version)
			continue ()
		endif()
		string(REGEX REPLACE "^#define[\t ]+JPEG_LIB_VERSION[\t ]+([0-9]+).*" "\\1" JPEG_VERSION "${jpeg_lib_version}")
		break ()
	endforeach ()
	unset(jpeg_lib_version)
	unset(_JPEG_CONFIG_HEADER)
	unset(_JPEG_CONFIG_HEADERS)
	unset(_JPEG_CONFIG_HEADERS_FEDORA)
	unset(_JPEG_CONFIG_HEADERS_DEBIAN)
endif ()
find_library(
	JPEG_LIBRARY
	NAMES jpeg libjpeg
	HINTS ${JPEG_LIBRARY_DIRS}
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
	JPEG
	REQUIRED_VARS JPEG_INCLUDE_DIR JPEG_LINK_LIBRARIES
	VERSION_VAR JPEG_VERSION
)

if (JPEG_FOUND)
	set(JPEG_LIBRARIES ${JPEG_LINK_LIBRARIES})
	set(JPEG_INCLUDE_DIRS "${JPEG_INCLUDE_DIR}" ${JPEG_INCLUDE_DIRS})
	if (NOT TARGET JPEG::JPEG)
		add_library(JPEG::JPEG UNKNOWN IMPORTED)
		set_target_properties(
			JPEG::JPEG PROPERTIES
			INTERFACE_INCLUDE_DIRECTORIES "${JPEG_INCLUDE_DIRS}"
		)
		if (EXISTS "${JPEG_LIBRARY}")
			set_target_properties(
				JPEG::JPEG PROPERTIES
				IMPORTED_LINK_INTERFACE_LANGUAGES "C"
				IMPORTED_LOCATION "${JPEG_LIBRARY}"
			)
		endif()
	endif()
endif ()
